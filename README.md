# Reto Reactjs Frontend  - (API para películas) 📽


¡Hola!, te damos la bienvenida a este reto para formar parte de la familia @[LUDIK](https://ludik.pe/)!

¿Alguna vez te has encontrado en una situación en la que no sabías qué película ver? El reto consistirá en crear una aplicación web para buscar películas. Con la función _Peli Random_ los usuarios podrán obtener fácilmente las películas recomendadas según el género que prefieran.

## Concepto e historias de usuario
Te brindaremos una idea del concepto que queremos con sus respectivas historias de usuario, sin embargo, te pedimos que seas creativo, puedes usar el estilo que prefieras.

### Pantalla principal
Esto es una referencia de cómo debería quedar la pantalla principal

![Preview](./images/main-screen.png)


* Como usuario, quiero poder desplazarme por la lista de películas. En la lista debería darme una breve información sobre cada película, incluido el título de la película, la imagen de portada, el año, la calificación y el idioma.
* Como usuario, quiero poder hacer clic en la película específica y acceder a la página con todos los detalles sobre la película elegida.
* Como usuario, quiero hacer clic en el botón "CARGAR" (LOAD) al final de la lista para cargar más películas.
* Como usuario quiero poder abrir el modal de `Peli Random` haciendo clic en el botón flotante en la esquina inferior derecha.

### Detalles de la película

![Preview](./images/movie-details.png)

* Como usuario, quiero obtener algunos detalles sobre una película específica, incluido el título de la película, la imagen de portada, el año, la descripción, la clasificación de popularidad, el idioma y las compañías de producción.
* Como usuario quiero calificar una película haciendo clic en el ícono de estrella (0-10).
* Como usuario, quiero ver mi calificación si ya califiqué una película específica.

### Modal de Peli Random

![Preview](./images/roulette-modal.png)

* Como usuario, quiero obtener una película aleatoria por género y ver sus detalles porque no estoy seguro de qué ver

### Entregable 📋
+ Enviarnos un enlace del repositorio que debamos clonar a clau@ludik.pe.
+ Incluir los pasos de configuración del proyecto en un README.md para que podamos ejecutar el proyecto

### Reglas y sugerencias 🧐
* Usar el [API para películas](https://developers.themoviedb.org/3/getting-started/introduction)
* Puede escribir su aplicación en JS puro o usar la biblioteca de su elección: React (preferido) o Vue
* Los estilos deben estar escritos en Sass (SCSS) o  [CSS in JS](https://medium.com/dailyjs/what-is-actually-css-in-js-f2f529a2757)
* Use un repositorio desde el principio, no suba todo el proyecto en un solo commit.
* Preste especial atención a la calidad del código, el formato y las mejores prácticas.
* Te brindamos un día (6hrs) para realizar el entregable

### Puntos extra 🌟
* No usar el kit de inicio como [create-react-app](https://github.com/facebook/create-react-app)
* Usando el verificador de tipo ([Typescript](https://www.typescriptlang.org/) or [Flow](https://flow.org/))
* Usando linter
* La aplicación está implementada y activa
* Se evaluará el tiempo de envío de la prueba VS el tiempo en que nos envías tu entregable 🕒

### Millas extra 🥇
* [Snapshot testing](https://jestjs.io/docs/en/snapshot-testing)
* [Storybook UI documentation](https://storybook.js.org)
* Optimizaciones de rendimiento
* Características sorprendentes que no esperamos obtener 😉

___

**¡Éxitos y cualquier duda, siéntete libre de escribirnos! 💫**

* Claudia Urcia Castillo (Jefe de desarrollo)  - clau@ludik.pe
